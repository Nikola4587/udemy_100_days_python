from turtle import Turtle

STARTING_POSITION = (0, -280)
MOVE_DISTANCE = 10
FINISH_LINE_Y = 280
SHAPE = "turtle"
COLOR = "black"


class Player(Turtle):
    def __init__(self):
        super().__init__()
        self.create_player()

    def create_player(self):
        self.shape(SHAPE)
        self.color(COLOR)
        self.penup()
        self.setheading(90)
        self.go_to_starting_position()

    def go_up(self):
        self.forward(MOVE_DISTANCE)

    def go_to_starting_position(self):
        self.goto(STARTING_POSITION)

    def is_at_finish_line(self):
        if self.ycor() > 280:
            return True
        else:
            return False
