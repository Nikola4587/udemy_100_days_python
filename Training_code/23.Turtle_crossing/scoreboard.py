from turtle import Turtle


FONT = ("Courier", 24, "normal")


class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.actual_level = 1
        self.hideturtle()
        self.color("black")
        self.penup()
        self.update_level()

    def update_level(self):
        self.goto(x=-280, y=250)
        self.write(arg=f"Level: {self.actual_level}", move=True, font=FONT)

    def increase_level(self):
        self.actual_level += 1
        self.clear()
        self.update_level()

    def game_over(self):
        self.goto(x=0, y=0)
        self.write(arg="Game Over", align="center", move=True, font=FONT)



