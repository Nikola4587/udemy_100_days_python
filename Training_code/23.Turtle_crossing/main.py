import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard
import random


screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)

player = Player()
car_manager = CarManager()
scoreboard = Scoreboard()

screen.listen()  # Listen to the key from keyboard
screen.onkey(fun=player.go_up, key="Up")


game_is_on = True
speed = 0.1
while game_is_on:
    time.sleep(speed)
    screen.update()
    car_manager.create_cars()
    car_manager.move_cars()

    # Detect collision with car
    for car_position in car_manager.all_cars:
        if car_position.distance(player) < 20:
            game_is_on = False
            scoreboard.game_over()

    # Detect if turtle cross to the other side:
    if player.is_at_finish_line():
        player.go_to_starting_position()
        car_manager.speed_up()
        scoreboard.increase_level()


screen.exitonclick()









