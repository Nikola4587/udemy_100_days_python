import random
from functions import play_guessing_game

difficulties = {
    "easy": 10,
    "hard": 5,
}

print("Welcome to the Number Guessing Game")
print("I am thinking of a number between 1 and 100.")
number_to_guess = random.randint(1, 100)  # Chose the random number between 1 and 100
print(f"Psssst. The correct number is {number_to_guess}")  # show the answer for debug


user_difficulty = input("Choose a difficulty. Type 'easy' or 'hard': ").lower()
if user_difficulty == "easy" or user_difficulty == "hard":  # check if user choose the good option
    number_of_attempts = difficulties[user_difficulty]  # we are using dictionary to take the numbers of attemps
    play_guessing_game(number_of_attempts, number_to_guess)

else:
    print("You did not choose the good option. Bye bye")



