


def play_guessing_game(number_of_attempts, number_to_guess):
    user_answer = 0
    while user_answer !=number_to_guess:
        print(f"You have {number_of_attempts} attempts remaining to guess the number.")
        user_answer = int(input("Make a guess"))
        number_of_attempts = check_answer(number_of_attempts, number_to_guess, user_answer)

        if number_of_attempts == 0:
            print("You loose")
            return
        elif user_answer != number_to_guess:
            print("Guess again.")


def check_answer(number_of_attempts, number_to_guess, user_answer):
    if number_to_guess > user_answer and number_of_attempts > 0:
        number_of_attempts -= 1
        print("Too low.")
        return number_of_attempts

    elif number_to_guess < user_answer:
        number_of_attempts -= 1
        print("Too high.")
        return number_of_attempts

    else:
        print(f"You got it! The answer was {number_to_guess}")





