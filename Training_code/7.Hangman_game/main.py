from calendar import c
from mimetypes import guess_all_extensions
import random
from this import d
from turtle import pos
from hangman_art import logo, stages
from hangman_words import word_list

chossen_word = random.choice(word_list)
chossen_word_lenght = len(chossen_word)

end_of_game = False
lives = 6
counter = 0

""" When the code is started:
1. Show the logo from hangman_art
2. For debug, show the word that has been chossen by utilisateur"""
print(logo)
print(f"Pssst, the solution is {chossen_word}")

"""1st step of the game is to print as many blanks as the chossen word have the letters."""
display = []
for letter_position in range(chossen_word_lenght):
    display.append("_") 


while not end_of_game:
    """2nd step: ask the user for a letter"""
    guess_letter = input("Please choose a letter").lower()

    # Check if the user have been already chosen this letter
    if guess_letter in display:
        print("You have already guessed this letter. Please choose other one!")

    # Check guessed letter
    for letter in chossen_word:
        counter += 1
        if guess_letter == letter:
            display[counter - 1] = guess_letter

    # If the guessed letter is wrong
    if guess_letter not in chossen_word:
        print(f"{guess_letter} not in chosen word")
        lives -= 1

    if lives == 0:
        end_of_game = True
        print("You lose.")

    print(stages[lives])
    print(display)
    # Check if the user guess all the letters:
    if "_" not in display:
        print("You won")
        end_of_game = True

    counter = 0
    
print(display)


