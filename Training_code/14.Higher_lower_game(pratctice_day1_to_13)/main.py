from art import logo
from art import vs

from game_data import data
from functions import generate_random_number, check_user_choice


user_win = True
final_score = 0

compare_a = generate_random_number()
compare_b = generate_random_number()

print(logo)
while user_win == True:

    print(f"Compare A: {data[compare_a]['name']}, a {data[compare_a]['description']}, from {data[compare_a]['country']}")
    print(vs)
    print(f"Against B: {data[compare_b]['name']}, a {data[compare_b]['description']}, from  {data[compare_b]['country']}")
    user_choice = input("Who has more followers. Type 'A' or 'B': ")
    user_win = check_user_choice(user_choice=user_choice, compare_a=compare_a, compare_b=compare_b)

    if user_win is True:
        final_score += 1
        if user_choice == "A":
            compare_b = generate_random_number()
        elif user_choice == "B":
            compare_a = generate_random_number()
        print(f"You are right!. Current score: {final_score}")
    else:
        print(f"Sorry, that is wrong. Final score: {final_score}")

