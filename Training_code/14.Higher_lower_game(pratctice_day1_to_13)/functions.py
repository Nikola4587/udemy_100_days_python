from random import randint
from game_data import data


def generate_random_number():
    random_number = randint(0, len(data))
    return random_number


def check_user_choice(user_choice, compare_a, compare_b):
    a_follower_count = (data[compare_a]['follower_count'])
    b_follower_count = (data[compare_b]['follower_count'])
    user_win = False
    if user_choice == "A":
        if a_follower_count > b_follower_count:
            user_win = True
        else:
            user_win = False

    elif user_choice == "B":
        if b_follower_count > a_follower_count :
            user_win = True
        else:
            user_win = False

    return user_win




