# Calculator
from art import logo



def add(number1, number2):
    """Adding function. Takes 2 numbers and return the addition"""
    return number1 + number2


def subtract(number1, number2):
    """Subtract function. Takes 2 numbers and return the subtracted number"""
    return number1 - number2


def multiply(number1, number2):
    """Multiply function. Takes 2 numbers and return the multiplied number"""
    return number1 * number2


def divide(number1, number2):
    """Divide function. Takes 2 numbers and return the divided number"""
    if number2 == 0:
        return "We can not divide by 0"
    return number1 / number2

operations = {
    "+": add,
    "-": subtract,
    "*": multiply,
    "/": divide,
}


def calculator():
    print(logo)
    continue_calculation = True
    num1 = float(input("What is the first number ?"))
    while continue_calculation is True:
        for key in operations:
            print(key)
        operation_symbol = input("Pick an operation from the line above: ")
        num2 = float(input("What is the next number ?"))
        answer = operations[operation_symbol](num1, num2)
        print(f"{num1} {operation_symbol} {num2} = {answer}")

        ask_user_to_continue_calculation = input(f"Type 'y' to continue calculating with {answer}, or type 'n' to start"
                                                 f"the new calculation: ")
        if ask_user_to_continue_calculation == "y":
            num1 = answer
            continue_calculation = True
        else:
            continue_calculation = False
            calculator()  # This is calling recursion


calculator()
