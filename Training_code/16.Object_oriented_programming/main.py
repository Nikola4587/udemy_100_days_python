from menu import Menu, MenuItem
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine

is_on = True

# Construct the object
my_menu = Menu()
my_coffee_maker = CoffeeMaker()
my_money_machine = MoneyMachine()


def play_game():
    is_on = True

    while is_on is True:
        coffee_choice = my_menu.get_items()  # get choices for coffee machine
        user_choice = input(f"What would you like ? {coffee_choice}").lower()
        if user_choice == "report":
            my_coffee_maker.report()
            my_money_machine.report()
        elif user_choice == "off":
            is_on = False
        else:
            drink_chosen = my_menu.find_drink(order_name=user_choice)
            if my_coffee_maker.is_resource_sufficient(drink=drink_chosen) is True:
                if my_money_machine.make_payment(cost=drink_chosen.cost):
                    my_coffee_maker.make_coffee(drink_chosen)


play_game()



