import pandas


# TODO: Create a table named squirrel_count.csv
# Prinary Pur Color : Cinnamon, Gray, Black
# How many of each color do we have in the table

datas = pandas.read_csv("2018_Central_Park_Squirrel_Census_-_Squirrel_Data.csv")
colors = datas["Primary Fur Color"]

red = datas[datas["Primary Fur Color"] == "Cinnamon"]
gray = datas[datas["Primary Fur Color"] == "Gray"]
black = datas[datas["Primary Fur Color"] == "Black"]

red_count = red.shape[0]  # Take only the row count
gray_count = gray.shape[0]  # Take only the row count
black_count = black.shape[0]  # Take only the row count
data_dict = {
    "Fur Color": ["grey", "red", "black"],
    "Count": [gray_count, red_count, black_count]
}

data_to_save = pandas.DataFrame(data_dict)
data_to_save.to_csv("squirrel_count.csv")
print(data_dict)
