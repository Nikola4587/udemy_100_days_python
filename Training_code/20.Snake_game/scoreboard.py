from turtle import Turtle
ALIGNMENT = "center"
FONT = ('Courier', 12, 'normal')


class ScoreBoard(Turtle):
    def __init__(self):
        super().__init__()
        with open(file="data.txt", mode="r") as file:
            self.high_score = int(file.read())
        self.actual_score = 0
        # self.high_score = 0
        self.hideturtle()
        self.color("white")
        self.penup()
        self.update_score_board()

    def update_score_board(self):
        self.clear()
        self.goto(x=0, y=280)
        self.write(arg=f"Score: {self.actual_score} High Score: {self.high_score}", move=True, align=ALIGNMENT, font=FONT)

    def reset(self):
        if self.actual_score > self.high_score:
            self.high_score = self.actual_score
            with open(file="data.txt", mode="w") as file:
                file.write(f"{str(self.high_score)}")

        self.actual_score = 0
        self.update_score_board()

    def increase_score(self):
        self.actual_score += 1
        self.update_score_board()


