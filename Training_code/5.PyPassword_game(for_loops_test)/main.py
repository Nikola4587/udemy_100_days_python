import random
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

password_letters_string = ""
password_symbols_string = ""
password_numbers_string = ""

print("Welcome to the PyPassword Generator")
letters_in_the_password_int = int(input("How many letters would you like in your password?"))
symbols_in_the_password_int = int(input("How many symbols would you like in your password?"))
numbers_in_the_password_int = int(input("How many numbers would you like in your password?"))

# Create the string with random numbers, symbols and letters
random_letters_str = random.sample(letters, letters_in_the_password_int)

random_symbols_str = random.sample(symbols, symbols_in_the_password_int)

random_number_str = random.sample(numbers, numbers_in_the_password_int)

password_list = random_number_str + random_symbols_str + random_letters_str

random.shuffle(password_list)

password = ""
for char in password_list:
    password += char
print(f"Your password is {password}")

