import random
from my_module import rock, paper, scissors


# Write your code below this line 👇
user_choise_int = int(input("What do you choose ? \n "
      "Type 0 for Rock, 1 for Paper or 2 for Scissors"))

if user_choise_int <=3 and user_choise_int >=0:
    formes_list_str = [rock, paper, scissors]
    print("Your choice is", formes_list_str[user_choise_int])


    computer_choice_int = random.randint(0, 2)
    print("Computer choise is", formes_list_str[computer_choice_int])

    # Update the list by priority
    formes_to_win_by_priority = [rock, scissors, paper]


    # Rock wins against scissors.

    if computer_choice_int > user_choise_int and computer_choice_int !=1:
        print("You win")
    if computer_choice_int == user_choise_int:
        print("Its a drow")
    else:
        print("You lose")
else:
    print("Invalid number, please retry")


