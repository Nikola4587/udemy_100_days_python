import pandas

# TODO 1. Create a dictionary in this format:
# {"A": "Alfa", "B": "Bravo"}

alphabet = pandas.read_csv(filepath_or_buffer="nato_phonetic_alphabet.csv")

nato_alphabet_dict = {row.letter: row.code for (index, row) in alphabet.iterrows()}

# TODO 2. Create a list of the phonetic code words from a word that the user inputs.


# while user_input is not nato_alphabet_dict:
def generate_phonetic():
    user_input = input("Enter a word:").upper()

    try:
        result = [nato_alphabet_dict[letter] for letter in user_input]

    except KeyError:
        print("Sorry, only letters in alphabet please")
        generate_phonetic()

    else:
        print(result)


generate_phonetic()

