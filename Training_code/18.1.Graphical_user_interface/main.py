import turtle
from turtle import Turtle, Screen
import random

angles = [0, 90, 180, 270]
number_sides = range(3, 10)

turtle.colormode(255)  # Allow us to change the colors using RGB


def draw_square():
    for iteration in range(4):
        my_turtle.forward(100)
        my_turtle.right(90)


def dashed_line():
    for iteration in range(15):
        my_turtle.pendown()
        my_turtle.forward(10)
        my_turtle.penup()
        my_turtle.forward(10)


def design_forme(sides, color):
    my_turtle.color(color)
    for it in range(sides):
        my_turtle.forward(100)
        angle = 360 / sides
        my_turtle.right(angle)


def random_walk(angle, color):
    my_turtle.color(color)
    my_turtle.setheading(angle)
    my_turtle.forward(30)


def random_color():
    random_red = random.randint(0, 255)
    random_green = random.randint(0, 255)
    random_blue = random.randint(0, 255)
    colors_tuple = (random_red, random_green, random_blue)
    return colors_tuple


def draw_spirograph(size_of_gap):
    for it in range(int(360 / size_of_gap)):
        my_turtle.color(random_color())
        my_turtle.circle(100)
        current_heading = my_turtle.heading()
        my_turtle.setheading(current_heading + size_of_gap)


my_turtle = Turtle()

"""1st exercise: Design random forme"""
# for iteration in number_sides:
#     random_color = random.choice(colors)
#     design_forme(sides=iteration, color=random_color)


"""2nd exercise: Generate random walk"""
# for iteration in range(100):
#     random_angle = random.choice(angles)
#     random_walk(angle=random_angle, color=random_color())

"""3rd exercise: Draw spirograph"""
my_turtle.speed(0)
my_turtle.pensize(1)
draw_spirograph(size_of_gap=10)



screen = Screen()
screen.exitonclick()



