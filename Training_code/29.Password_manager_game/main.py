import tkinter
from tkinter import END
from tkinter import messagebox
import random
import pyperclip  # copy board package
import json

WHITE = 'white'
# ---------------------------- PASSWORD GENERATOR ------------------------------- #
def find_password():
    """."""
    user_website = website_entry.get()

    try:
        with open(file="data.json", mode="r") as file:
            # Try to read all data
            json_data = json.load(file)
        info_logging = json_data[user_website]
        messagebox.showinfo(title=user_website, message=f"Email: {info_logging['email']}\n Password: {info_logging['password']}: ")
    except FileNotFoundError:
        messagebox.showinfo(title="Error", message="No Data File Found")
    except KeyError:
        messagebox.showinfo(title="Error", message="No details for website exist")



def generate_password():

    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

    letters_list = [random.choice(letters) for _ in range(random.randint(8, 10))]

    passwords_list = [random.choice(symbols) for _ in range(random.randint(2, 4))]

    numbers_list = [random.choice(numbers) for _ in range(random.randint(2, 4))]

    final_list = letters_list + passwords_list + numbers_list
    random.shuffle(final_list)

    password = ""
    for char in final_list:
      password += char
    # password = "".join(final_list)
    password_entry.insert(0, string=password)
    pyperclip.copy(text=password) # copy the password

# ---------------------------- SAVE PASSWORD ------------------------------- #
def save_data():
    website_entered = website_entry.get()
    email_entered = email_entry.get()
    password_entered = password_entry.get()
    new_data = {
        website_entered: {
            "email": email_entered,
            "password": password_entered,
        }
    }

    if len(website_entered) == 0 or len(password_entered) == 0:
        messagebox.showinfo(title="Ooops", message="Please dont leave any fields empty!")

    else:
        try:
            with open(file="data.json", mode="r") as file:
                # Try to read all data
                data = json.load(file)
        except FileNotFoundError:
            # If we have exception, we will create a new json file
            with open(file="data.json", mode="w") as data_file:
                json.dump(data_file, file, indent=4)
        else:
            # Updating old data with new data
            data.update(new_data)

            # Saving updated data
            with open(file="data.json", mode="w") as file:
                json.dump(data, file, indent=4)  # indent is just to make easier to read


        finally:
            # Delete website and password entries
            website_entry.delete(0, END)
            password_entry.delete(0, END)

# ---------------------------- UI SETUP ------------------------------- #

# Create a window
my_window = tkinter.Tk()
my_window.title("Password Manager")
my_window.config(padx=50, pady=50)


# LOGO Image Canvas management
canvas_image = tkinter.Canvas(width=200, height=200, bg=WHITE, highlightthickness=0) # highlightthickness=0 est le countour de la photo
logo_image = tkinter.PhotoImage(file="logo.png")
canvas_image.create_image(100, 100, image=logo_image)  # x and y values. Allow us to center the image.
canvas_image.grid(row=0, column=1)

# Website Label management
website_label = tkinter.Label(text="Website:")
website_label.grid(row=1, column=0)

# Website Entry management
website_entry = tkinter.Entry(width=32)
website_entry.grid(row=1, column=1)
website_entry.focus()  # focus the cursor on particular entry


# Email Label management
email_label = tkinter.Label(text="Email/Username:")
email_label.grid(row=2, column=0)

# Email Entry management
email_entry = tkinter.Entry(width=50)
email_entry.insert(0, string="nikola.dimitrijevic@etml-es.ch")
print(email_entry.get())
email_entry.grid(row=2, column=1, columnspan=2)

# Password Label management
password_label = tkinter.Label(text="Password:")
password_label.grid(row=3, column=0)

# Password Entry management
password_entry = tkinter.Entry(width=32)
password_entry.grid(row=3, column=1)

# Generate password button
generate_password_button = tkinter.Button(text="Generate Password", command=generate_password)
generate_password_button.grid(row=3, column=2)

# Add button
add_button = tkinter.Button(text="Add", width=43, command=save_data)
add_button.grid(row=4, column=1, columnspan=2)

# Search button
search_button = tkinter.Button(text='Search', width=15, command=find_password)
search_button.grid(row=1, column=2, columnspan=2)

my_window.mainloop()
