import turtle
import pandas
retry = False

screen = turtle.Screen()
screen.title("U.S. States Game")

image = "blank_states_img.gif"  # path to image
screen.addshape(image)  # add shape to the screen
turtle.shape(image)  # change turtle to image previously downloaded
guested_states = []
non_guested_states = []

csv_data = pandas.read_csv(filepath_or_buffer="50_states.csv")
all_state_list = csv_data.state.to_list()

while len(guested_states) <= 50:
    # Check if user wrote exit:

    answer_user = screen.textinput(title=f"{len(guested_states)}/50 States Correct",
                                   prompt="What is another state's name?").capitalize()

    # if answer_user == "Exit":
    #     # Save the missing state
    #     for state in all_state_list:
    #         if state not in guested_states:
    #             non_guested_states.append(state)
    #
    #     data_to_save = pandas.DataFrame(non_guested_states)
    #     data_to_save.to_csv("states_to_learn.csv")
    #     break
    if answer_user == "Exit":
        non_guested_states = [state for state in all_state_list if state not in guested_states]
        data_to_save = pandas.DataFrame(non_guested_states)
        data_to_save.to_csv("states_to_learn.csv")
        break

    # 1. Check if answer user is in data frame:
    if answer_user in csv_data.state.values:
        guested_states.append(answer_user)
        coordinates_x_y = csv_data[csv_data.state == answer_user]

        my_turtle = turtle.Turtle()
        my_turtle.hideturtle()
        my_turtle.penup()
        my_turtle.goto(x=int(coordinates_x_y.x), y=int(coordinates_x_y.y))
        my_turtle.write(answer_user)



screen.exitonclick()