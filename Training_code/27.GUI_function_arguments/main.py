import tkinter


def mile_to_km_converter(miles):
    miles_float = float(miles)
    result = round(miles_float * 1.609)
    return str(result)  # return float value


def calculate_button_clicked():
    miles = user_miles_value_entered.get()
    user_value_converted_to_km = mile_to_km_converter(miles)
    value_converted_to_km.config(text=user_value_converted_to_km)


# Window management
my_window = tkinter.Tk()
my_window.title("Mile to Km Converter")
my_window.config(padx=20, pady=20)
my_window.minsize(width=300, height=100)  # Precise the minimum size of the window.

# User miles value management
user_miles_value_entered = tkinter.Entry(width=7)
user_miles_value_entered.grid(column=1, row=0)

# Text miles management
text_miles = tkinter.Label(text="Miles")
text_miles.grid(column=2, row=0)

# Is equal to management
is_equal_to_text = tkinter.Label(text="is equal to")
is_equal_to_text.grid(column=0, row=1)

# Text result label
value_converted_to_km = tkinter.Label(text="0")
value_converted_to_km.grid(column=1, row=1)

# Calculate button management
calculate_button = tkinter.Button(text="Calculate", command=calculate_button_clicked)
calculate_button.grid(column=1, row=2)

my_window.mainloop()  # Keep the window on screen