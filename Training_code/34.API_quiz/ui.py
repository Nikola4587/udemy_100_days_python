import tkinter
from quiz_brain import QuizBrain


THEME_COLOR = "#375362"
WHITE = "white"
FONT_NAME = "Arial"


class QuizInterface:

    def __init__(self, quiz_brain: QuizBrain):
        self.quiz = quiz_brain
        self.window = tkinter.Tk()
        self.window.title("Quizzler")
        self.window.config(padx=20, pady=20, background=THEME_COLOR)

        # Result label
        self.result_label = tkinter.Label(text="Score:0", background=THEME_COLOR, fg="white")
        self.result_label.grid(column=1, row=0)  # pack, grid or place

        # Text canvas
        self.canvas_question = tkinter.Canvas(width=300, height=250, bg=WHITE,
                                              highlightthickness=0)  # highlightthickness=0 est le countour de la photo

        self.question_text = self.canvas_question.create_text(
            150,
            125,
            text="Test",
            fill="black",
            width='280',
            font=(FONT_NAME, 20, "italic"))
        self.canvas_question.grid(column=0, row=1,  columnspan=2, padx=20, pady=50)

        # OK Image
        answer_ok_image = tkinter.PhotoImage(file="images/true.png")
        self.button_true = tkinter.Button(image=answer_ok_image,
                                          highlightthickness=0,
                                          command=self.user_answer_true)  # x and y values. Allow us to center the image.
        self.button_true.grid(column=0, row=2)

        # OK Image

        answer_false_image = tkinter.PhotoImage(file="images/false.png")
        self.button_false = tkinter.Button(image=answer_false_image,
                                           highlightthickness=0,
                                           command=self.user_answer_false)  # x and y values. Allow us to center the image.
        self.button_false.grid(column=1, row=2)

        # Print question
        self.get_next_question()
        self.window.mainloop()

    def get_next_question(self):
        self.canvas_question.config(bg="white")
        if self.quiz.still_has_questions():
            self.result_label.config(text=f"Score: {self.quiz.score}")
            question_text = self.quiz.next_question()
            self.canvas_question.itemconfig(self.question_text, text=question_text)
        else:
            self.canvas_question.itemconfig(self.question_text, text="Game OVer")
            self.button_true.config(state="disabled")  # Disable the buttons
            self.button_false.config(state="disabled")  # Disable the buttons


    def user_answer_true(self):
        is_right = self.quiz.check_answer(user_answer="True")
        self.give_feedback(is_right=is_right)

    def user_answer_false(self):
        is_right = self.quiz.check_answer(user_answer="False")
        self.give_feedback(is_right=is_right)

    def give_feedback(self, is_right):
        if is_right is True:
            self.canvas_question.config(bg="green")
            self.window.after(1000, func=self.get_next_question)

        else:
            self.canvas_question.config(bg="red")
            self.window.after(1000, func=self.get_next_question)


