from logos import treasure
print("Welcome to Treasure Island.")
print(treasure) # print treasure logo
print("Your mission is to find the treasure.")

left_or_right_input_str = input("Left or Right?") 
left_or_right_input_str_lower = left_or_right_input_str.lower()

if left_or_right_input_str_lower == 'left':
    swim_or_wait_input_str = input("Swim or Wait ?")
    swim_or_wait_input_str_lower = swim_or_wait_input_str.lower()
    if swim_or_wait_input_str_lower == 'wait':
        which_door_input_str = input("Which door color ?")
        which_door_input_str_lower = which_door_input_str.lower()
        if which_door_input_str == 'red':
            print("Burned by the fire. Game over")
        elif which_door_input_str == 'blue':
            print("Eaten by beasts. Game over")
        elif which_door_input_str == 'yellow':
            print("You win!")
        else:
            print("Game Over")

    else:
        print("Attacked by trout. Game Over")
else:
    print("Fall into a hole. Game over")

# Organigramme: https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Treasure%20Island%20Conditional.drawio#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1oDe4ehjWZipYRsVfeAx2HyB7LCQ8_Fvi%26export%3Ddownload

#Write your code below this line 👇
