import turtle

import colorgram
import random
from turtle import Turtle, Screen
color_list = [(240, 245, 241), (236, 239, 243), (149, 75, 50), (222, 201, 136), (53, 93, 123), (170, 154, 41), (138, 31, 20), (134, 163, 184), (197, 92, 73), (47, 121, 86), (73, 43, 35), (145, 178, 149), (14, 98, 70), (232, 176, 165), (160, 142, 158), (54, 45, 50), (101, 75, 77), (183, 205, 171), (36, 60, 74), (19, 86, 89), (82, 148, 129), (147, 17, 19), (27, 68, 102), (12, 70, 64), (107, 127, 153), (176, 192, 208), (168, 99, 102)]


"""Generation of colors from image"""
# colors = colorgram.extract('image.jpg', 30)
# rgb_colors = []
# for color in colors:
#     red = color.rgb.r
#     green = color.rgb.g
#     bleu = color.rgb.b
#     new_color = (red, green, bleu)
#     rgb_colors.append(new_color)


"""Generate Hirst painting"""
my_turtle = Turtle()
turtle.colormode(255)  # Allow us to change the colors using RGB
my_turtle.hideturtle()
NUMBER_OF_POINTS_X = 10
NUMBER_OF_POINTS_Y = 10
SPACE_BETWEEN_POINTS = 50

for y in range(NUMBER_OF_POINTS_Y):
    y_position = y * SPACE_BETWEEN_POINTS
    for x in range(NUMBER_OF_POINTS_X):
        x_position = x*SPACE_BETWEEN_POINTS
        my_turtle.penup()
        my_turtle.setpos(x_position, y_position)
        my_turtle.pendown()
        my_turtle.color(random.choice(color_list))
        my_turtle.dot(20)


screen = Screen()
screen.exitonclick()
