# If the bill was $150.00, split between 5 people, with 12% tip.
# Each person should pay (150.00 / 5) * 1.12 = 33.6
# Format the result to 2 decimal places = 33.60

# Tip: There are 2 ways to round a number. You might have to do some Googling to solve this.💪

# Write your code below this line 👇
print("Welcome to the tip calculator.")

total_bill_str = input("What was the total bill? $")
total_bill_float = float(total_bill_str)

user_tip_str = input("What percentage ti p would you like to give ? 10, 12 or 15 ?")
user_tip_int = int(user_tip_str)

numbers_of_people_str = input("How many people split the bill ?")
numbers_of_people_int = int(numbers_of_people_str)

total_bill_per_personne_float = float(total_bill_float / numbers_of_people_int)
tips_per_personne_float = float(total_bill_float / numbers_of_people_int) * (user_tip_int / 100)

need_to_pay_per_personne_int = total_bill_per_personne_float + tips_per_personne_float
need_to_pay_per_personne_int_rounded = round(need_to_pay_per_personne_int, 2)

print(f"Each person should pay: ${need_to_pay_per_personne_int_rounded}")
