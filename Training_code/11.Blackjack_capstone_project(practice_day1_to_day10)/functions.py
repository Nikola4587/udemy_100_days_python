import random
cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]


def deal_card(number_of_cards):
    # if the number of cards is 1, we will return int
    final_cards = random.sample(cards, k=number_of_cards)
    if number_of_cards == 1:
        for carte in final_cards:
            return carte
    return final_cards


def calculate_score(list_of_cards):
    final_score = 0
    for number in list_of_cards:
        final_score += number

    # Check if someone has a blackjack
    if final_score == 21 and len(list_of_cards) == 2:
        return 0
    else:
        # If the ace counts as a 1 instead of 11, are they still over 21?
        if 11 in list_of_cards and final_score > 21:
            list_of_cards.remove(11)  # remove 11
            list_of_cards.append(1)  # put 1 in place
    return final_score



def compare_results(user_score, computer_score):
    if user_score > 21 and computer_score > 21:
        return "You went over. You lose 😤"
    if user_score == computer_score:
        return "Draw 🙃"
    elif computer_score == 0:
        return "Lose, opponent has Blackjack 😱"
    elif user_score == 0:
        return "Win with a Blackjack 😎"
    elif user_score > 21:
        return "You went over. You lose 😭"
    elif computer_score > 21:
        return "Opponent went over. You win 😁"
    elif user_score > computer_score:
        return "You win 😃"
    else:
        return "You lose 😤"
