from art import logo
from functions import calculate_score, deal_card, compare_results


def play_game():
    is_game_over = False
    print(logo)
    user_cards = deal_card(number_of_cards=2)  # Give 2 cards to user
    computer_cards = deal_card(number_of_cards=2)  # Give 2 cards to computer

    while is_game_over is False:
        user_score = calculate_score(user_cards)
        computer_score = calculate_score(computer_cards)

        print(f"Your cards:{user_cards}, current score: {user_score}")
        print(f"Computer's first card: {computer_cards[0]}")

        if user_score > 21:
            is_game_over = True
        else:
        # Ask the user if they want to get another card.
            user_answer_get_another_card = input("Type 'y' to get another card, type 'n' to pass:\n")
            if user_answer_get_another_card == "y":
                new_user_carte = deal_card(1)
                user_cards.append(new_user_carte)
            else:
                is_game_over = True
    while computer_score != 0 and computer_score < 17:
        new_carte_computer = deal_card(number_of_cards=1)
        computer_cards.append(new_carte_computer)
        computer_score = calculate_score(computer_cards)

    print(f"Your final hand:{user_cards}, final score: {user_score}")
    print(f"Computer's final hand: {computer_cards}, final score: {computer_score}")
    print(compare_results(user_score, computer_score))


while input("Do you want to play a game of Blackjack ? Type 'y' or 'n' :") == "y":
    play_game()
else:
    print("You don't want to play the game. Bye bye!")





