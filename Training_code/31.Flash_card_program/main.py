BACKGROUND_COLOR = "#B1DDC6"

import tkinter
import pandas
import random
import time

datas = pandas.read_csv(filepath_or_buffer="data/french_words.csv")
to_learn = datas.to_dict(orient="records")

current_card = {}


def choose_random_word():
    """."""
    global current_card, flip_timer
    window.after_cancel(flip_timer)
    current_card = random.choice(to_learn)
    canvas.itemconfig(card_title, text="French")
    canvas.itemconfig(card_word, text=current_card["English"])
    canvas.itemconfig(card_background, image=card_front_image)
    window.after(3000, func=flip_card)  # ater 3 seconds, we will flip the card


def flip_card():
    """."""

    canvas.itemconfig(card_title, text="English", fill="white")
    canvas.itemconfig(card_word, text=current_card["English"], fill="white")
    canvas.itemconfig(card_background, image=card_back_image)


window = tkinter.Tk()
window.title("Flashy")
window.config(padx=50, pady=50, bg=BACKGROUND_COLOR)
flip_timer = window.after(3000, func=flip_card)  # ater 3 seconds, we will flip the card


# Create the images
canvas = tkinter.Canvas(width=800, height=526)
card_front_image = tkinter.PhotoImage(file="images/card_front.png")
card_back_image = tkinter.PhotoImage(file="images/card_back.png")
card_background = canvas.create_image(400, 263, image=card_front_image)  # x and y values. Allow us to center the image.
card_title = canvas.create_text(400, 150, text="", fill="black", font=("Ariel", 40, "italic"))
card_word = canvas.create_text(400, 253, text="", fill="black", font=("Ariel", 60, "bold"))
canvas.config(bg=BACKGROUND_COLOR, highlightthickness=0)

canvas.grid(column=0, row=0, columnspan=2)

# Create the button
button_green = tkinter.PhotoImage(file="images/right.png")
button_red = tkinter.PhotoImage(file="images/wrong.png")

button_g = tkinter.Button(image=button_green, command=choose_random_word)
button_g.grid(column=1, row=2)

button_r = tkinter.Button(image=button_red, command=choose_random_word)
button_r.grid(column=0, row=2)


choose_random_word()
window.mainloop()
