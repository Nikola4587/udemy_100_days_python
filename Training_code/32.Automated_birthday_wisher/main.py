##################### Extra Hard Starting Project ######################
import datetime as dt
import random
import pandas
import smtplib

sender = "udemypythonproject@gmail.com"
birthday_person_mail = ""
password = "fbtlwbyzmvirylji"
# 1. Update the birthdays.csv
# 2. Check if today matches a birthday in the birthdays.csv

# Today day and time
now = dt.datetime.now()
today_day = now.day
today_month = now.month
today = (today_day, today_month)
random_letter = random.randint(1, 3)

# Read birthday CSV
data = pandas.read_csv(filepath_or_buffer="birthdays.csv")
birthday_dict = {(data_row.day, data_row.month): data_row for (index, data_row) in data.iterrows()}

# 3. If step 2 is true, pick a random letter from letter templates and replace the [NAME] with the person's actual
# name from birthdays.csv
if today in birthday_dict:
    birthday_person = birthday_dict[today]["name"]
    birthday_person_mail = birthday_dict[today]["email"]
    with open(file=f"letter_templates/letter_{random_letter}.txt") as file:  # open random letter
        letter_content = file.read()
        letter_to_send = letter_content.replace("[NAME]", birthday_person)


# 4. Send the letter generated in step 3 to that person's email address.
with smtplib.SMTP("smtp.gmail.com") as connection:
    connection.starttls()
    connection.login(user=sender, password=password)
    connection.sendmail(
        from_addr=sender,
        to_addrs=birthday_person_mail,
        msg=f"Subject: Happy birthday!!!\n\n{letter_to_send}",)



