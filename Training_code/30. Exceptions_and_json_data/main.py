# # FileNotFound error
# try:
#     file = open("a_file.txt")
#     a_dictionnary = {"key": "value"}
#     print(a_dictionnary["key"])
#
# except FileNotFoundError:
#     file = open("a_file.txt", "w")  # If the file is not existing we will create it.
#
# except KeyError as error_message:
#     print(f"This key {error_message} is no exist")
#
# else:
#     content = file.read()
#     print(content)
#
# finally:
#     raise TypeError("This is an error that I made up.")


# Rising our own exceptions


# It is not recommanded at all to keep except emplty. In this case we have the key error in dictionnary and we will go directly to except, so
# the key error will be ignored.
# In the exception to catch a specific situation.


#
# # KeyError
# a_dictionary = {"key": "value"}
# value = a_dictionary["non_existing_key"]
#
# # IndexError
# fruit_list = ["Apple", "Banana", "Pear"]
# fruit = fruit_list[3]

# # TypeError
# text = 'abc'
# print(text + 5)

# When something goes wrong and in that moment we catch that exception whe can decide what should happen

# Keywords
# try: (excecute something that migh cause an exception)
# except:
# else:
# finally:

# height = float(input("Height"))
# weight = int(input("Weight:"))
#
# bmi = weight / (height ** 2)
#
# if height > 3:
#     raise ValueError("Human Height should not be over 3 meters.")
fruits = ["Apple", "Pear", "Orange"]

#TODO: Catch the exception and make sure the code runs without crashing.

def make_pie(index):
    fruit = fruits[index]
    print(fruit + " pie")

try:
    make_pie(4)

except IndexError:
    print("Fruit pie")

else:
    print("None")
