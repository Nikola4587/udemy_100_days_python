from turtle import Turtle

SHAPE = "square"
COLOR = "white"
WIDTH = 1
LENGTH = 5


class Paddle(Turtle):
    def __init__(self, position):
        super().__init__()
        self.position = position
        self.create_paddle()

    def create_paddle(self):
        self.shape(SHAPE)
        self.color(COLOR)
        self.turtlesize(stretch_len=LENGTH, stretch_wid=WIDTH)
        self.setheading(90)
        self.penup()
        self.goto(self.position)

    def go_up(self):
        self.forward(20)

    def go_down(self):
        self.backward(20)
