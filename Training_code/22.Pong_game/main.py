import turtle
from turtle import Screen
from paddle import Paddle
from ball import Ball
import time
from score_board import Scoreboard

SCREEN_BG_COLOR = "black"
SCREEN_TITLE = "Pong"


game_is_on = True

# Setup screen
screen = Screen()
screen.setup(width=800, height=600)
screen.tracer(0)  # turn off the animation
screen.bgcolor(SCREEN_BG_COLOR)
screen.title(SCREEN_TITLE)


right_paddle = Paddle(position=(350, 0))
left_paddle = Paddle(position=(-350, 0))
ball = Ball()
score = Scoreboard()

screen.listen()  # Listen to the key from keyboard
screen.onkey(fun=right_paddle.go_up, key="Up")
screen.onkey(fun=right_paddle.go_down, key="Down")
screen.onkey(fun=left_paddle.go_up, key="w")
screen.onkey(fun=left_paddle.go_down, key="s")

time_sleep = 0.1
while game_is_on:
    time.sleep(ball.move_speed)
    screen.update()
    ball.move()

    # Detect collision with wall
    if ball.ycor() > 280 or ball.ycor() < -280:
        ball.bounce_y()

    # Detect the collision with right paddle
    if ball.distance(right_paddle) < 50 and ball.xcor() > 320 or ball.distance(left_paddle) < 50 and ball.xcor() < -320:
        ball.bounce_x()

    # Detect right side mises
    if ball.xcor() > 380:
        ball.reset_position()
        score.left_point()

    # Detect left side mises
    if ball.xcor() < -380:
        ball.reset_position()
        score.right_point()






screen.exitonclick()
