MENU = {
    "espresso": {
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    "latte": {
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    "cappuccino": {
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    }
}

resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
}


def print_report(total_money):
    """This function allows us to print the report"""
    print(f"Water: {resources['water']}ml\n"
          f"Milk: {resources['milk']}ml\n"
          f"Coffee: {resources['coffee']}g\n"
          f"Money: ${total_money}\n")


def remove_resources(user_choice):
    """If we have the resources, we will remove them to make new drink"""
    for keys in resources.keys():
        if user_choice is not "espresso" and keys is not "milk":
            resources[keys] = resources[keys] - MENU[user_choice]["ingredients"][keys]


def check_resources(user_choice):
    """Check resources to be sure that we can make the coffee"""
    for keys in resources.keys():
        if user_choice is not "espresso" and keys is not "milk":  # espresso doesn't have the milk
            actual_resources = resources[keys] - MENU[user_choice]["ingredients"][keys]
            if actual_resources <= 0:
                print(f"Sorry there is not enough {keys}")
                return True  # True means we will need to restart the game. We don't have enough resources,

    return False  # Else we dont need to restart the game


def process_coins():
    """Ask user for coins and calculate the total"""
    print("Please insert coins")
    quarters = int(input("how many quarters ?: \n"))
    dimes = int(input("how many dimes ?: \n"))
    nickles = int(input("how many nickles ?: \n"))
    pennies = int(input("how many pennies ?: \n"))

    dollars = ((0.25 * quarters) + (0.1 * dimes) + (0.05 * nickles) + (0.01 * pennies))
    return dollars


def check_if_user_have_enough_money(user_choice, user_inserted_coins):
    """If the user have enough money, the resources will be removed and the cost of coffee will be returned"""
    coffee_cost = MENU[user_choice]["cost"]

    if user_inserted_coins - coffee_cost >= 0:
        # user put enough money
        remove_resources(user_choice)  # we can remove resources

        difference = user_inserted_coins - coffee_cost
        difference_rounded = round(difference, 2)
        print(f"“Here is ${difference_rounded} dollars in change.")
        return coffee_cost  # return coffee cost to calculate total money in machine

    elif user_inserted_coins - coffee_cost < 0:
        # user don't have enough money
        print("Sorry that's not enough money. Money refunded.")
        return False





