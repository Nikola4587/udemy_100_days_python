from functions import print_report
from functions import check_if_user_have_enough_money
from functions import check_resources
from functions import process_coins

end_of_run = False  # End of run will be used in the case user would like to turn off the coffee machine
total_money = 0  # Amount of money in coffee machine
restart_game = False  # We will replay game if machine doesn't have enough resources or
# user doesn't have enough money


def play_game(total_amount_of_money):
    """Play the machine coffee game"""
    end_of_game = False

    while end_of_game is False:
        user_choice = input("What would you like? (espresso/latte/cappuccino):")

        if user_choice == "off":  # turn off the machine is user choose off
            print("The coffee machine is turned off ")
            end_of_game = True

        if user_choice == "report":  # print the report if user choose report
            print_report(total_amount_of_money)
            # When the report is printed, we will replay the game so the user can choose drink
            play_game(total_amount_of_money)

        # Check resources sufficient?
        restart_game = check_resources(user_choice)

        # TODO: 5. Process coins.
        if restart_game is False:  # that means that we have sufficient resources
            user_inserted_coins = process_coins()
            coffee_cost = check_if_user_have_enough_money(user_choice, user_inserted_coins)

            if coffee_cost is False:  # User doesn't have enough money
                play_game(total_amount_of_money)  # We will restart the game
            else:
                total_amount_of_money += coffee_cost  # Calulate the total
            print(f"Here is your {user_choice}. Enjoy!”.")

        if restart_game is True:
            play_game(total_amount_of_money)
    return end_of_game


while end_of_run is False:
    end_of_run = play_game(total_money)
