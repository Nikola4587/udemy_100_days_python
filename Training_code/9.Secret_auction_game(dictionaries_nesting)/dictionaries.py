import os
from art import logo
print(logo)
print("Welcome to the secret auction program.")
end_of_game = False

my_dict={}
highest_score = 0
person_win = ""
score_win = 0


while end_of_game == False:
    user_name = input("What is your name ?")
    user_bid_price = input("What is your bid ?")

    my_dict.update({user_name: user_bid_price}) # Update the dic with new values

    other_users_question = input("Are there any other bidders? Type 'yes' or 'no'")
    if other_users_question == "yes":
        end_of_game = False
    elif other_users_question == "no":
        end_of_game = True
        for person in my_dict:  # if the answer is no, it means that we don't have more players
            score = (my_dict[person])
            if int(score) > highest_score:  # find the highest score
                highest_score = int(score)
                person_win = person
            else:
                highest_score = 0

        print(f"The winner is {person_win} with a bid of {str(highest_score)}")

    else:
        print("You chose the wrong answer. End of the game")

