# TODO: Create a letter using starting_letter.txt
with open(file="Input/Letters/starting_letter.txt") as file:
    letter = file.read() # read the letter

# for each name in invited_names.txt
with open(file="Input/Names/invited_names.txt", mode="r") as file: # take invited names
    list_of_names = file.read().splitlines()  # put the names in a list

    # Replace the [name] placeholder with the actual name.
    for name in list_of_names:
        new_letter = letter.replace("[name]", name)  # Replace [name] by the name from a list.

        # Save the letters in the folder "ReadyToSend".
        path_to_save_txt = f"Output/ReadyToSend/'letter_for_{name}.txt'"
        with open(file=path_to_save_txt, mode="w") as file_to_write:
            file_to_write.write(new_letter)

    print(new_letter)







    
#Hint1: This method will help you: https://www.w3schools.com/python/ref_file_readlines.asp
    #Hint2: This method will also help you: https://www.w3schools.com/python/ref_string_replace.asp
        #Hint3: THis method will help you: https://www.w3schools.com/python/ref_string_strip.asp