# main.py
import art
from functions import caesar
print(art.logo)  # At the begging of the program, print the logo

continue_to_play = 1
user_error = 0
while continue_to_play == 1:
    direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n").lower()
    if direction != "encode" and direction != "decode":
        for retry in range(5):
            direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n").lower()
            if direction == "encode" or direction == "decode":
                break
            else:
                continue
        print("Please restart the game")
        user_error = 1

    if user_error == 0:
        text = input("Type your message:\n").lower()
        shift = int(input("Type the shift number:\n"))
        shift = shift % 26
        caesar(start_text=text, shift_amount=shift, cipher_direction=direction)

        answer_user = input("Type 'yes' if you want to go again. Otherwise type 'no'").lower()
        if answer_user == "yes":
            continue_to_play = 1
        elif answer_user == "no":
            continue_to_play = 0
            print("Good bye")
        else:
            print("You don't want to go again. The program will end")
    else:
        print("End of the game. User chose wrong inpout")
        continue_to_play = 0



